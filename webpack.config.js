const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  devtool: 'eval',
  context: path.resolve(__dirname, 'app'),
  entry: './index.tsx',
  output: {
    filename: '[name].bundle.js',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js']
  },
  module: {
    rules: [
      { test: /\.tsx?$/, use: 'ts-loader'}
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({ template: './assets/index.html' })
  ]
}