import * as React from "react";
import styled from "styled-components";

const StyledComponent = styled.div`
  background: red;
  height: 100px;
  width: 100px;
`;

const YellowButton = styled.button`
  height: 40px;
  background: yellow;
  border-radius: 10px;
  outline: none;
`;

export class App extends React.Component {

  handlerButton = ({ phrase = "", v = 5 }): void => {
    console.log(`saymon said ${ phrase }, v = ${ v + 1 } `);
  }

  render() {
    return (
      <div>
        <h2>nice</h2>
        <StyledComponent />
        <YellowButton 
          onClick={() => this.handlerButton({ phrase: 'hello people' })}
        >
          Im in gucci. im so pretty
        </YellowButton>
      </div>
    );
  }
}